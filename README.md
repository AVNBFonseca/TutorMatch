# TutorMatch

## You need to have instaled:
python 2.7  
pip  
SQLite3 (on ubuntu sqlite3 libsqlite3-dev)  
git  

## Installation
On a command line do the following

### Create virtual environment
virtualenv env  
source env/bin/activate  

### Install Python dependencies
pip install Flask Flask-SQLAlchemy Flask-Restful Flask-Jsonpify  
 
 
### Get code from GitLab 
git clone https://gitlab.com/AVNBFonseca/TutorMatch.git  
cd TutorMatch  

* Alternatively unzip the code you received and enter the main folder

### Create SQLite db
sqlite3 tutors.db  
sqlite> create table tutors (  
   tutor_id integer PRIMARY KEY,  
   name text NOT NULL,  
   city text NOT NULL,  
   phone text NOT NULL,  
   teaches_subject text NOT NULL  
   );
* press Ctrl+D to exit sqlite3  



## Running  
 
### Start app
python tutors.py

### curl test post:
curl --header "Content-Type: application/json"   --request POST   --data '{"Name":"Carlos Santos", "City":"Porto", "Phone":"223456789", "TeachesSubject":"Programing"}' http://127.0.0.1:5002/tutors  

### curl test get
curl http://127.0.0.1:5002/tutors  

### curl test delete
curl --header "Content-Type: application/json"   --request DELETE   --data '{"ID":1}' http://127.0.0.1:5002/tutors  

### Access UI
On a web browser, browse to http://127.0.0.1:5002/  

On the page you will see a Table listing all the tutors, a "List tutors" button that refreshes the list and a form to add a new tutor.  
To add a new tutor all fields must be filled