from flask import Flask, request, jsonify, render_template
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps

db_connect = create_engine('sqlite:///tutors.db')
app = Flask(__name__)
api = Api(app)

class Tutors(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute("select * from tutors")
        return {'tutors': [i for i in query.cursor.fetchall()]} 

    def post(self):
        conn = db_connect.connect()
        print request.json
        Name = request.json['Name']
        City = request.json['City']
        Phone = request.json['Phone']
        TeachesSubject = request.json['TeachesSubject']
        query = conn.execute("insert into tutors values(null,'{0}','{1}','{2}','{3}' \
                             )".format(Name, City, Phone, TeachesSubject))
        return {'status':'success'}

    def delete(self):
        conn = db_connect.connect()
        print request.json
        ID = request.json['ID']
        query = conn.execute("delete from tutors where tutor_id = {0}".format(ID))
        return {'status':'success'}


api.add_resource(Tutors, '/tutors')

@app.route("/")
def home():
    return render_template("index.html")

if __name__ == '__main__':
     app.run(port='5002')
